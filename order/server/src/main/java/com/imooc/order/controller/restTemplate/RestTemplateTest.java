package com.imooc.order.controller.restTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RestTemplateTest {
    
    @GetMapping("/rest/msg1")
    public String getMsg1() {
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject("http://localhost:8081/msg", String.class);
        return response;
    }
    
    @Autowired
    private LoadBalancerClient loadbalancerClient;
    
    @GetMapping("/rest/msg2")
    public String getMsg2() {
        RestTemplate restTemplate = new RestTemplate();
        ServiceInstance serviceInstance = loadbalancerClient.choose("PRODUCT");
        String url = String.format("http://%s:%s", serviceInstance.getHost(), serviceInstance.getPort() + "/msg");
        String response = restTemplate.getForObject(url, String.class);
        return response;
    }
    
    @Autowired
    private RestTemplate restTemplate;
    
    @GetMapping("/rest/msg3")
    public String getMsg3() {
        String response = restTemplate.getForObject("http://PRODUCT/msg", String.class);
        return response;
    }
}
