package com.imooc.order.message;

import com.fasterxml.jackson.core.type.TypeReference;
import com.imooc.order.utils.JsonUtil;
import com.imooc.product.common.ProductInfoOutput;
import com.rabbitmq.tools.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@Slf4j
public class ProductInfoReceive {
    
    @RabbitListener(queuesToDeclare = @Queue("productInfo"))
    public void process(String message){
        List<ProductInfoOutput> productInfoOutputList = JsonUtil.jsonToObject(message, new TypeReference<List<ProductInfoOutput>>() {
        });
        log.info("[productInfo] receive message : {}", message);
        
        //TODO 将商品信息存入redis中
        //由于是微服务,可能有多个order服务在运行,要加redis锁
    }
}
