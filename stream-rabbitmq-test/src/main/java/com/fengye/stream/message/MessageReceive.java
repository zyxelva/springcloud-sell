package com.fengye.stream.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(Sink.class)
@Slf4j
public class MessageReceive {
    
    @StreamListener(Sink.INPUT)
    public void handle(String message) {
        log.info("Received: {}", message);
    }
}
