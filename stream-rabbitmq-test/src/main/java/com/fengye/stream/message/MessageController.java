package com.fengye.stream.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@Slf4j
@EnableBinding(Source.class)
public class MessageController {
    
    @Autowired
    private Source source;
    
    @GetMapping("/sendMessage")
    public String process() {
        String message = "now is " + new Date();
        boolean result = source.output().send(MessageBuilder.withPayload(message).build());
        if (result) {
            log.info("success send message : {}" , message);
            return "发送成功";
        }
        return "nothing,try again!!!";
    }
}
