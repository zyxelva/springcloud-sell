package com.imooc.product.client;

import com.imooc.product.common.DecreaseStockInput;
import com.imooc.product.common.ProductInfoOutput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
@Slf4j
public class ProductClientFallback implements ProductClient {
    @Override
    public List<ProductInfoOutput> listForOrder(final List<String> productIdList) {
        log.debug("请求发生了错误,{}","数据未获取到");
        return Collections.emptyList();
    }
    
    @Override
    public void decreaseStock(final List<DecreaseStockInput> decreaseStockInputList) {
        log.debug("减库存,{}","数据库请求失败");
    }
}
