package com.fengye.user.service.impl;

import com.fengye.user.dataobject.UserInfo;
import com.fengye.user.repository.UserInfoRepository;
import com.fengye.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author fengyexjtu@126.com
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserInfoRepository userInfoRepository;
    @Override
    public UserInfo findByOpenid(final String openid) {
        return userInfoRepository.findByOpenid(openid);
    }
}
