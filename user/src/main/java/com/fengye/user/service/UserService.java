package com.fengye.user.service;

import com.fengye.user.dataobject.UserInfo;

/**
 * @author fengyexjtu@126.com
 */
public interface UserService {
    /**
     * 通过openid查询用户
     * @param openid
     * @return
     */
    UserInfo findByOpenid(String openid);
}
