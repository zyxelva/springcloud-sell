package com.fengye.user.controller;

import com.fengye.user.common.*;
import com.fengye.user.constant.CookieConstant;
import com.fengye.user.constant.RedisConstant;
import com.fengye.user.dataobject.UserInfo;
import com.fengye.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author fengyexjtu@126.com
 */
@RestController
@Slf4j
public class UserController {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    
    @GetMapping("/buyer")
    public ResultVO buyer(@RequestParam String openid, HttpServletResponse response) {
        UserInfo userInfo = userService.findByOpenid(openid);
        if (userInfo == null) {
            return ResultVOUtil.error(ResultEnum.LOGIN_FAIL);
        }
        log.info("code:{},role:{}", RoleEnum.BUYER.getCode(), userInfo.getRole());
        if (!RoleEnum.BUYER.getCode().equals(userInfo.getRole())) {
            return ResultVOUtil.error(ResultEnum.ROLL_ERROR);
        }
        // 设置cookie
        CookieUtil.setCookie(response, CookieConstant.OPENID, openid, CookieConstant.EXPIRE_TIME);
        return ResultVOUtil.success();
    }
    
    @GetMapping("/seller")
    public ResultVO seller(@RequestParam String openid, HttpServletRequest request, HttpServletResponse response) {
        
        //判断是否已登录
        Cookie cookie = CookieUtil.getCookie(request, CookieConstant.TOKEN);
        if (cookie != null && !StringUtils.isEmpty(stringRedisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_TEMPLATE, cookie.getValue())))) {
            log.info("已登录");
            return ResultVOUtil.success();
        }
        UserInfo userInfo = userService.findByOpenid(openid);
        if (userInfo == null) {
            return ResultVOUtil.error(ResultEnum.LOGIN_FAIL);
        }
        log.info("code:{},role:{}", RoleEnum.SELLER.getCode(), userInfo.getRole());
        if (!RoleEnum.SELLER.getCode().equals(userInfo.getRole())) {
            return ResultVOUtil.error(ResultEnum.ROLL_ERROR);
        }
        Integer expireTime = CookieConstant.EXPIRE_TIME;
        // 写redis
        String uuid = UUID.randomUUID().toString();
        String format = String.format(RedisConstant.TOKEN_TEMPLATE, uuid);
        stringRedisTemplate.opsForValue().set(
                format,
                openid,
                expireTime,
                TimeUnit.SECONDS
        );
        
        // 设置cookie
        CookieUtil.setCookie(response, CookieConstant.TOKEN, uuid, CookieConstant.EXPIRE_TIME);
        return ResultVOUtil.success();
    }
}
