package com.fengye.user.repository;

import com.fengye.user.dataobject.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author fengyexjtu@126.com
 *
 */
public interface UserInfoRepository extends JpaRepository<UserInfo,String> {
    /**
     * 通过openid查询单个用户
     * @param openid 微信openid
     * @return 用户信息
     */
    UserInfo findByOpenid(String openid);
}
