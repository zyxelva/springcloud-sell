package com.fengye.user.common;

import lombok.Getter;

/**
 * @author fengyexjtu@126.com
 */
@Getter
public enum RoleEnum {
    /**
     * 买家
     */
    BUYER(1,"买家"),
    /**
     * 卖家
     */
    SELLER(2,"卖家"),

    ;

    private Integer code;

    private String message;

    RoleEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}