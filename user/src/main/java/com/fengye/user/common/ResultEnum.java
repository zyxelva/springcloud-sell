package com.fengye.user.common;

import lombok.Getter;

@Getter
public enum ResultEnum {
    /**
     * 登录失败
     */
    LOGIN_FAIL(1,"登录失败"),
    /**
     * 登录成功
     */
    LOGIN_SUCCESS(2,"登录成功"),
    /**
     * 登录角色不对应
     */
    ROLL_ERROR(3,"角色有误")
    ;

    private Integer code;

    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}