package com.fengye.user.repository;

import com.fengye.user.UserApplicationTests;
import com.fengye.user.dataobject.UserInfo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import static org.junit.Assert.*;

/**
 * @author fengyexjtu@126.com
 */
@Component
public class UserInfoRepositoryTest extends UserApplicationTests {
    @Autowired UserInfoRepository userInfoRepository;
    @Test
    public void findByOpenid() {
        UserInfo userInfo = userInfoRepository.findByOpenid("abc");
        Assert.notNull(userInfo,"数据查询结果:哼哼,没查到");
    }
}