package com.fengye.rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitmqTestApplicationTests {

    @Autowired
    private AmqpTemplate amqpTemplate;
    @Test
    public void contextLoads() {
    }
    
    @Test
    public void send(){
        amqpTemplate.convertAndSend("myQueue","this is a message");
    }
    
    @Test
    public void sendToComputer(){
        amqpTemplate.convertAndSend("myExchange","computer","this is a message");
    }
    
    @Test
    public void sendToPhone(){
        amqpTemplate.convertAndSend("myExchange","phone","this is a message");
    }

}
