package com.fengye.rabbitmq.message;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ReceiveMessage {
    
    // @RabbitListener(queues = "myQueue")
    // @RabbitListener(queuesToDeclare = @Queue("myQueue"))//自动创建
    @RabbitListener(bindings = @QueueBinding(exchange = @Exchange("myExchange"),value = @Queue("myQueue")))//自动创建,并且自动绑定
    public void receiveMessage(String message){
        log.info("receiveMessage:{}",message);
    }
    
    @RabbitListener(bindings = @QueueBinding(exchange = @Exchange("myExchange"),value = @Queue("myComputerQueue"),key = "computer"))//自动创建,并且自动绑定,使用key将队列进行分组
    public void receiveComputerMessage(String message){
        log.info("receiveComputerMessage:{}",message);
    }
    
    @RabbitListener(bindings = @QueueBinding(exchange = @Exchange("myExchange"),value = @Queue("myPhoneQueue"),key = "phone"))//自动创建,并且自动绑定
    public void receivePhoneMessage(String message){
        log.info("receivePhoneMessage:{}",message);
    }
}
